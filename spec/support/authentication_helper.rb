module AuthenticationHelpers
  def login_with(user)

    visit new_user_session_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Sign in'
  end

  def sign_out
    visit root_path
    click_on "Logout"
  end

  def sign_up_with(user_attrs)
    visit new_user_registration_path

    fill_in "user[email]",               with: user_attrs[:email]
    fill_in "user[password]",                 with: "123123123"
    fill_in "user[password_confirmation]",    with: "123123123"

    find_button("Sign up").click
  end
end