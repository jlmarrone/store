require 'spec_helper'

feature 'User Sign Up' do
  before :each do
    visit new_user_registration_path
  end

  scenario 'creates a valid account' do

    fill_in 'Email',                 with: "user.email@gmail.com"
    fill_in 'Password',              with: "12345678"
    fill_in 'Password confirmation', with: "12345678"
    click_button 'Sign up'

    expect(page).to have_content 'Products'
  end

  scenario 'fails to create account' do

    fill_in 'Email',                 with: ''
    fill_in 'Password',              with: ''
    fill_in 'Password confirmation', with: ''
    click_button 'Sign up'

    expect(page).to have_content 'errors prohibited this user from being saved'
  end
end