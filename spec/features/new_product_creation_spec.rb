require 'spec_helper'

feature 'Products Creation' do
  before :each do
    user = create(:user)
    login_with(user)

    visit new_product_path
  end

  scenario 'with valid attributes' do

    expect{
      fill_in('product[name]',  :with => 'New Product 1')
      fill_in('product[price]', :with => 10)
      attach_file('product[avatar]',
        File.expand_path("spec/fixtures/capybara.jpg"))
      click_button 'Create Product'
    }.to change(Product, :count).by(1)

    expect(page).to have_content 'Product was successfully created.'
    expect(page).to have_content 'Product'

    within '.dl-horizontal' do
      expect(page).to have_content 'New Product 1'
      expect(page).to have_content 10
    end
  end

  scenario 'with invalid attributes' do
    fill_in('product[name]',  :with => '')
    fill_in('product[price]', :with => '')

    click_button "Create Product"

    expect(page).to have_content 'errors prohibited this user from being saved'
  end
end