require 'spec_helper'

feature 'User visits products lists page' do
  before :each do
    user = create(:user)
    login_with(user)
  end

  scenario 'complete products list' do
    product = FactoryGirl.create(:product)

    click_link 'Browse Products'

    expect(page).to have_content 'Products'

    within 'table' do
      expect(page).to have_content product.name
      expect(page).to have_content product.price
    end
  end
end