require 'spec_helper'

feature 'User Sign In' do
  scenario 'logs in successfully' do
    user = create(:user)

    visit new_user_session_path

    fill_in 'Email',                 with: user.email
    fill_in 'Password',              with: user.password
    click_button 'Sign in'

    expect(page).to have_content 'Products'
  end

  scenario 'fails to log in' do
    user = create(:user)

    visit new_user_session_path

    fill_in 'Email',                 with: ''
    fill_in 'Password',              with: ''
    click_button 'Sign in'

    expect(page).to have_content 'Invalid email or password.'
  end
end