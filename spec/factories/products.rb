FactoryGirl.define do
  factory :product do
    name                 { Faker::Lorem.sentence }
    price                10
    avatar_file_name     "file.png"
    avatar_content_type  "image/png"
    avatar_file_size     "11386"
    avatar_updated_at    Time.now
  end
end