//= require jquery
//= require jquery_ujs
//= require bootstrap


$(document).ready(function() {
 $("#form-preview").on("click", function(e) {
   e.preventDefault();
   var form = $("#new_sweepstake"),
    oldAction = form.attr("action");

   form.attr("target", "_blank");
   form.attr("action", "/admin/preview");
   form.submit();
   form.attr("action", oldAction);
   form.attr("target", "");
 });

 Metronic.init();
 Layout.init();

 $(".input-group.date").datepicker({
   format: 'yyyy-mm-dd'
 });
});
